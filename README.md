# Tesseract Data Trainer

A small python program to create .traineddata files for tesseract

## Requirements
Python3.5+ (Only tested with python 3.9.7)

Tesseract

Linux? Probably it will run on windows if you change the executable paths but this is not tested.

## Usage

1. Adjust the script e.g. the font to your needs.
2. Put your images into the images folder.
3. Execute the program once with the `-b` flag.
	1. `python train_tesseract.py -b`
4. Check all .box files if the text was recognized correctly.
	1. If everythin is correct probably you don't need to train it
	2. Modifiy the .box files until everything is correct.
	3. [There is also a nice gui for this](https://github.com/zdenop/qt-box-editor)
5. Execute the program again with the `-t` flag
	1. `python train_tesseract.py -t`
6. Find the .tessdata file inside the output folder 
	
