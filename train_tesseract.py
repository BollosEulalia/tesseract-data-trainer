#!/usr/bin/env python
"""Creates .box files and .traineddata files for tesseract
"""

import os
import subprocess
import shutil
import argparse
import sys
import textwrap

__author__ = "Kilian Payer"
__copyright__ = "Copyright 2021, Kilian Payer"
__credits__ = ["Lukas Pucher"]
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Kilian Payer"
__email__ = ""
__status__ = "Beta"

# ----------------------------------------------------------------------------------------------------------------------
# Adjust these values to your needs. Probably you'll only need to change FONT_PROPERTIES.
#

#             fontname italic bold monospace serif fraktur
FONT_PROPERTIES = "roboto-mono 0 0 1 0 0"

# language prefix you want to have. default = eng. -> this will create eng.traineddata
LANG_PREFIX = "eng."

SHAPECLUSTERING = True

#input files - put all your images in this folder
IMAGEDIR="images" 

#  only this file formats will be checked in IMAGEDIR
IMAGEFORMATS= [".png", ".tif"] 

BOX_FILEENDING = [".box"]

TR_FILEENDING = [".tr"]

# images will be copied to this folder + .tr files will be created here
BOX_OUTDIR = "boxes"

# .tr files will be created inside this folder
TR_OUTDIR = BOX_OUTDIR #"tr_files"


# final output files e.g. .tessdata will be moved into this folder
FINAL_OUTDIR = "output"

# ----------------------------------------------------------------------------------------------------------------------


def fil_extension_valid(filename: str, fileendings: [str]):
    """Checks if filename has a valid extension

    Args:
        filename (str): filename
        fileendings ([type]): list with file extensions including . e.g. ".png"

    Returns:
        [bool]: True if fileextension is valid else False
    """
    for fileending in fileendings:
        if filename.endswith(fileending):
            return True
    return False

def move_files_to_final_outdir():
    """moves all created files to FINAL_OUTDIR
    """
    print("moving files to ", FINAL_OUTDIR)

    files = ["inttemp", "normproto", "pffmtable", "shapetable", "unicharset", "traineddata", "font_properties"]
    tmp  = [] + files
    for f in tmp:
        files.append(LANG_PREFIX + f)

    for f in files:
        if(os.path.exists(f)):
            shutil.move(f, os.path.join(FINAL_OUTDIR, f))

    pass

def rename_and_combine_files():
    """Renames outputfiles from mf/cntrain to <LANG_PREFIX>fileanme 
    and combines it to .tessdata file
    """
    print("renaming files and creating .traineddata")



    files = ["inttemp", "normproto", "pffmtable", "shapetable", "font_properties"]
    for f in files:
        if os.path.exists(f):

            if not os.path.exists(FINAL_OUTDIR):
                os.makedirs(FINAL_OUTDIR, exist_ok = True)

            try:
                new_filename = "{}{}".format(LANG_PREFIX, f)
                shutil.copyfile(f, new_filename)
                #shutil.copyfile(f, os.path.join(FINAL_OUTDIR, new_filename))
                #shutil.move(f, os.path.join(FINAL_OUTDIR, f))
            except:
                print("{} not found!".format(f))
        else:
            print("{} not found!".format(f))
    
    subprocess.run(["combine_tessdata", LANG_PREFIX])

def mf_and_cn_train():
    """executes mftrain and cntrain fo all .tr files
    """

    print("mftraining + cntraining")
    
    files = []
    tmp = os.listdir(TR_OUTDIR)

    for filename in tmp:
        if fil_extension_valid(filename, TR_FILEENDING):
            files.append( os.path.join(TR_OUTDIR, filename) )
        #files[i] = os.path.join(TR_OUTDIR, files[i])

    if not os.path.exists(TR_OUTDIR) and files:
        os.makedirs(TR_OUTDIR, exist_ok = True)

    if files:
        fontproperties = "font_properties"
        unicharset = "unicharset"
        #fontproperties = ""
        #unicharset = ""
        outfile = "{}{}".format(LANG_PREFIX, unicharset)
        print(" ".join(files))
        subprocess.run(["mftraining", "-F", fontproperties, "-U", unicharset, "-O",  outfile ] + files)
        subprocess.run(["cntraining"] + files)
    pass

def create_unicharset_and_fontproperties():
    """create font_properties and unicharset files required for mftrain and ctrain
    """
    print("creating unicharset and font_properties")
    
    files = []
    for f in os.listdir(BOX_OUTDIR):
        if fil_extension_valid(f, BOX_FILEENDING):
            files.append(os.path.join(BOX_OUTDIR, f))
    
    if files:
        subprocess.run(["unicharset_extractor" ] + files )

    with open("font_properties", "w+") as f:
        f.write(FONT_PROPERTIES + "\n")

    if SHAPECLUSTERING:
        for filename in files:
            subprocess.run(["shapeclustering", filename])

def create_tr_files():
    """Creates .tr files for each image
    """

    print("creating .tr files")

    for imagefile in os.listdir(BOX_OUTDIR):
        if fil_extension_valid(imagefile, IMAGEFORMATS):
            infile = os.path.join(BOX_OUTDIR, imagefile)
            outfile = os.path.join(os.path.join(TR_OUTDIR, os.path.splitext(imagefile)[0]))
            subprocess.run(["tesseract", infile, outfile, "nobatch", "box.train"])
    pass

def create_box_files(imagefiles: [str] ):
    
    """Creates .box files for each image and copies images into BOX_OUTDIR

    Args:
        imagefiles ([str]): list of filenames for all images
    """

    print("creating .box files")
    
    if not os.path.exists(BOX_OUTDIR) and imagefiles:
        os.makedirs(BOX_OUTDIR, exist_ok = True)

    for f in imagefiles:

        infile = os.path.join(IMAGEDIR, f)
        outfile = os.path.join(BOX_OUTDIR, os.path.splitext(f)[0])
        subprocess.run(["tesseract", infile, outfile, "makebox"])

        shutil.copyfile(infile, os.path.join(BOX_OUTDIR, f))

    return


def get_input_files() ->[str]:

    """Checks for valid images in IMAGEDIR

    Returns:
        [str]: list with filenames only. 
    """

    imagefiles = []

    for f in os.listdir(IMAGEDIR):
        if fil_extension_valid(f, IMAGEFORMATS):
            imagefiles.append(f)

    return imagefiles




def main(args) -> int:
    """depending on the cli flag it will either create .box files or <LANG_PREFIX>.traineddata

    Args:
        args ([ArgumentParser]): cli flags

    Returns:
        int: -1 if no input files found else 0
    """


    imagefiles = get_input_files()

    if len(imagefiles) < 1:
        print("no valid images found in ", IMAGEDIR)
        return -1

    if args.boxes:
        create_box_files(imagefiles)
    elif args.train:

        create_tr_files()

        create_unicharset_and_fontproperties()

        mf_and_cn_train()

        rename_and_combine_files()

        move_files_to_final_outdir()


    # for f in imagefiles:
    #     print(f)
    return 0


def create_argparser():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)



    parser.epilog = """Before running this program adjust the values of the beginning to your needs e.g. the font.
                       \nPut your images into {}.
                       \nFirst you need to run this program with the -b flag.
                       \nAfter alls .box files are generated into {} you should check if tesseract 
                         detected all characters correct of fix the .box files.
                       \nThen run it again with the -t flag. 
                       \nYour {}tessdata file should now be inside the {} folder.
                    """.format(IMAGEDIR, BOX_OUTDIR, LANG_PREFIX, FINAL_OUTDIR)

    parser.add_argument("--boxes", "--box", "-b", action="store_true", help="creates .box files")
    parser.add_argument("--train", "-t",action="store_true", help="creates .traineddata")

    return parser.parse_args()

if __name__ == "__main__":
    args = create_argparser()

    if not args.boxes and not args.train:
        print("you need to use -b or -t or nothing will happen!")
        exit(-1)

    exit(main(args))



